// libraries
import { PointerEvent, useCallback, useRef } from "react";

export interface Coord {
  x: number;
  y: number;
}

export interface DragInfo {
  /** The coordinates where the drag started, relative to the element's origin. */
  origin: Coord;
  /** The current drag coordinates, relative to the element's origin. */
  current: Coord;
  /** The distance dragged. */
  distance: Coord;
}

export type Handler<T> = (event: PointerEvent<T>, info: DragInfo) => void;

export interface Options<T extends Element> {
  /** Called when a drag operation begins. */
  onDragStart?(event: PointerEvent<T>, info: DragInfo): void;
  /** Called as the user drags. */
  onDrag?(event: PointerEvent<T>, info: DragInfo): void;
  /** Called when a drag operation ends. */
  onDragEnd?(event: PointerEvent<T>, info: DragInfo): void;
  /** The element relative to which coordinates should be calculated. */
  relativeTo?(node: T): Element;
}

export default function useHandle<T extends Element>(options: Options<T>) {
  const {
    onDragStart = noop,
    onDrag = noop,
    onDragEnd = noop,
    relativeTo = (node: T) => node
  } = options;

  // keep the drag start ref updated
  const dragStart = useRef<Handler<T>>(noop);
  dragStart.current = onDragStart;

  // keep the drag ref updated
  const drag = useRef<Handler<T>>(noop);
  drag.current = onDrag;

  // keep the dragEnd ref updated
  const dragEnd = useRef<Handler<T>>(noop);
  dragEnd.current = onDragEnd;

  // keep the relative ref updated
  const relative = useRef<(node: T) => Element>(node => node);
  relative.current = relativeTo;

  const origin = useRef<Coord | null>(null);

  const onPointerDown = useCallback((event: PointerEvent<T>) => {
    const target = event.currentTarget as T;
    target.setPointerCapture(event.pointerId);

    const { top, left } = relative.current(target).getBoundingClientRect();

    const { pageX, pageY } = event.nativeEvent;
    origin.current = {
      x: pageX - left,
      y: pageY - top
    };

    dragStart.current(event, {
      origin: origin.current,
      current: origin.current,
      distance: { x: 0, y: 0 }
    });
  }, []);

  const onPointerMove = useCallback((event: PointerEvent<T>) => {
    if (!origin.current) return;

    const target = event.currentTarget as T;
    const { top, left } = relative.current(target).getBoundingClientRect();

    const { pageX, pageY } = event.nativeEvent,
      start = origin.current;
    const current = { x: pageX - left, y: pageY - top };

    drag.current(event, {
      current,
      origin: start,
      get distance() {
        return {
          x: current.x - start.x,
          y: current.y - start.y
        };
      }
    });
  }, []);

  const onPointerUp = useCallback((event: PointerEvent<T>) => {
    const target = event.currentTarget as T;
    target.releasePointerCapture(event.pointerId);

    const { top, left } = relative.current(target).getBoundingClientRect();

    const { pageX, pageY } = event.nativeEvent,
      start = origin.current || { x: 0, y: 0 };
    const current = { x: pageX - left, y: pageY - top };

    dragEnd.current(event, {
      current,
      origin: start,
      get distance() {
        return {
          x: current.x - start.x,
          y: current.y - start.y
        };
      }
    });

    origin.current = null;
  }, []);

  return {
    onPointerDown,
    onPointerMove,
    onPointerUp
  };
}

function noop() {}
