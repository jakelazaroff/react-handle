# react-handle

A simple, performant React hook for making draggable UIs.

`react-handle` is a lightweight abstraction over the [pointer events API](https://developer.mozilla.org/en-US/docs/Web/API/Pointer_events). It won't actually move anything for you — instead, it handles the pointer event lifecycle. Use it as a small building block for creating graphics editors, games, timelines and more.

## Example

```tsx
import useHandle from "react-handle";

function CoolApp() {
  const handle = useHandle({
    onDrag(_, { distance }) {
      console.log(`Drag distance: ${distance.x}, ${distance.y}`);
    }
  });

  return <canvas {...handle} />;
}
```

## Options

`react-handle` consists of one function: the `useHandle` hook. It takes an object of callbacks — all optional — and returns props to be spread into the element the user will be dragging.

### onDragStart

> `function(event: PointerEvent, info: DragInfo)` | optional

Called when the drag operation starts. Passed the pointer event and the [DragInfo](#draginfo) object.

### onDrag

> `function(event: PointerEvent, info: DragInfo)` | optional

If a drag operation is in progress, called repeatedly as the pointer is moved. Passed the pointer event and the [DragInfo](#draginfo) object.

### onDragEnd

> `function(event: PointerEvent, info: DragInfo)` | optional

Called when the drag operation ends. Passed the pointer event and the [DragInfo](#draginfo) object.

### relativeTo

> `function(event: Element): Element` | optional

If you'd like the [DragInfo](#draginfo) coordinates to be relative to an element other than the dragged one, return that element from this function. Passed the event target (the dragged element).

## DragInfo

In addition to the pointer event, all drag callbacks are passed an object containing information about the drag operation.

### Origin

> `{ x: number; y: number; }`

The pointer coordinates when the drag operation began, relative to the top left corner of the element.

### Current

> `{ x: number; y: number; }`

The current pointer coordinates, relative to the top left corner of the element.

### Distance

> `{ x: number; y: number; }`

The difference between `current` and `origin`.
